<?php
    /*
    mysql> select *from prodi;
+----+------+--------------------+
| id | kode | nama               |
+----+------+--------------------+
|  1 | SI   | Sistem Informasi   |
|  2 | TI   | Teknik Informatika |
+----+------+--------------------+
2 rows in set (0,00 sec)


    */
    require_once "DAO.php";
    class Kegiatan extends DAO
    {
        public function __construct()
        {
            parent::__construct("prodi");
        }

        public function simpan($data){
            $sql = "INSERT INTO ".$this->tableName.
            " (id,kode,nama) ".
            " VALUES (default,?,?)";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }

        public function ubah($data){
            $sql = "UPDATE ".$this->tableName.
            " SET kode=? , nama=?".
            " WHERE id=?";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }
        //buat fungsi untuk menampilkan statistik

    }
?>
