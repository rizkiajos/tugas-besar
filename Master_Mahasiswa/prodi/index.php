<?php
include_once 'top.php';
require_once 'db/class_kegiatan.php';
?>
<h2>Daftar Prodi</h2>
<div class="panel-header">
    <a class="btn icon-btn btn-success" href="form_kegiatan.php">
    <span class="glyphicon btn-glyphicon glyphicon-plus img-
    circle text-success"></span>
    Tambah Prodi
    </a>
</div>
<?php
$obj = new Kegiatan();
$rows = $obj->getAll();
?>
<!-- Buat code javascript untuk memanggil table dan menggunakan fungsi datatable-->
<script languange="JavaScript">
    $(document).ready(function(){
        $('#example').DataTable();
    });
</script>
<table id="example" class="table table-striped table-bordered"><!-- Beri id pada tag table untuk dideteksi javascript-->
    <thead>
    <tr class="active">
        <th>Id</th><th>Kode</th><th>Nama</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $nomor = 1;
    foreach($rows as $row){
        echo '<tr><td>'.$nomor.'</td>';
        echo '<td>'.$row['kode'].'</td>';
        echo '<td>'.$row['nama'].'</td>';
        echo '<td><a href="view_kegiatan.php?id='.$row['id']. '">View</a> |';
        echo '<a href="form_kegiatan.php?id='.$row['id']. '">Update</a></td>';
        echo '</tr>';
        $nomor++;
    }
    ?>
    </tbody>
</table>
<?php
include_once 'bottom.php';
?>
