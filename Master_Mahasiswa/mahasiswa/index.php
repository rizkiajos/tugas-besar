<?php
include_once 'top.php';
require_once 'db/class_kegiatan.php';
?>
<h2>Daftar Mahasiswa</h2>
<div class="panel-header">
    <a class="btn icon-btn btn-success" href="form_kegiatan.php">
    <span class="glyphicon btn-glyphicon glyphicon-plus img-
    circle text-success"></span>
    Tambah Data Mahasiswa
    </a>
</div>
<br>
<div class="panel-header">
    <a class="btn icon-btn btn-success" href="grafik_kegiatan.php">
    <span class="glyphicon btn-glyphicon  img-
    circle text-success"></span>
    Grafik Prodi Mahasiswa
    </a>
</div>
<?php
$obj = new Kegiatan();
$rows = $obj->getAll();
?>
<!-- Buat code javascript untuk memanggil table dan menggunakan fungsi datatable-->
<script languange="JavaScript">
    $(document).ready(function(){
        $('#example').DataTable();
    });
</script>
<table id="example" class="table table-striped table-bordered"><!-- Beri id pada tag table untuk dideteksi javascript-->
    <thead>
    <tr class="active">
        <th>Nomor</th><th>NIM</th><th>Nama</th><th>Tempat Lahir</th><th>Tanggal Lahir</th><th>Jenis Kelamin</th><th>Prodi Id</th><th>IPK</th><th>Tahun Masuk</th><th>Email</th><th>Rombel Id</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $nomor = 1;
    foreach($rows as $row){
        echo '<tr><td>'.$nomor.'</td>';
        echo '<td>'.$row['nim'].'</td>';
        echo '<td>'.$row['nama'].'</td>';
        echo '<td>'.$row['tmp_lahir'].'</td>';
        echo '<td>'.$row['tgl_lahir'].'</td>';
        echo '<td>'.$row['jk'].'</td>';
        echo '<td>'.$row['prodi_id'].'</td>';
        echo '<td>'.$row['ipk'].'</td>';
        echo '<td>'.$row['thnmasuk'].'</td>';
        echo '<td>'.$row['email'].'</td>';
        echo '<td>'.$row['rombel_id'].'</td>';
        echo '<td><a href="view_kegiatan.php?id='.$row['nim']. '">View</a> |';
        echo '<a href="form_kegiatan.php?id='.$row['nim']. '">Update</a></td>';
        echo '</tr>';
        $nomor++;
    }
    ?>
    </tbody>
</table>
<?php
include_once 'bottom.php';
?>
