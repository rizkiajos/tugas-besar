<?php
    /*
    mysql> select * from mahasiswa;
+-----------+-------------------------------------+-----------+-----------+------+----------+------+----------+-------+-----------+
| nim       | nama                                | tmp_lahir | tgl_lahir | jk   | prodi_id | ipk  | thnmasuk | email | rombel_id |
+-----------+-------------------------------------+-----------+-----------+------+----------+------+----------+-------+-----------+
| 110116002 | Andhito Diaz Revandra               | NULL      | NULL      | L    |        1 | NULL |     2016 | NULL  |         1 |
| 110116007 | Maurice Yoga Ibrahim                | NULL      | NULL      | L    |        1 | NULL |     2016 | NULL  |         1 |
| 110116008 | Bayhaqi Alfaridzi                   | NULL      | NULL      | L    |        1 | NULL |     2016 | NULL  |         2 |
| 110116010 | Muhammad Hanif Dwi Cahya Ardiansyah | NULL      | NULL      | L    |        1 | NULL |     2016 | NULL  |         2 |
| 110116011 | Muhamad Idris                       | NULL      | NULL      | L    |        1 | NULL |     2016 | NULL  |         2 |
| 110116015 | Lizza Noor Azizah                   | NULL      | NULL      | L    |        1 | NULL |     2016 | NULL  |         2 |
| 110116016 | Fauzi Hafsar                        | NULL      | NULL      | L    |        1 | NULL |     2016 | NULL  |         2 |
| 110116017 | Lazuardi Dwi Putra                  | NULL      | NULL      | L    |        1 | NULL |     2016 | NULL  |         2 |
| 110116018 | Miftakhul Aris                      | NULL      | NULL      | L    |        1 | NULL |     2016 | NULL  |         2 |
| 110116019 | Chairin Nashrillah                  | NULL      | NULL      | P    |        1 | NULL |     2016 | NULL  |         2 |
| 110116023 | Elsa Nadira                         | NULL      | NULL      | L    |        1 | NULL |     2016 | NULL  |         2 |
| 110116026 | Lia Khairunnisa                     | NULL      | NULL      | L    |        1 | NULL |     2016 | NULL  |         2 |
| 110116028 | Rizka Amalia Apriliani              | NULL      | NULL      | L    |        1 | NULL |     2016 | NULL  |         2 |
| 110116030 | Shafura Iryani                      | NULL      | NULL      | P    |        1 | NULL |     2016 | NULL  |         2 |
| 110116032 | Nihlah Karimah                      | NULL      | NULL      | P    |        1 | NULL |     2016 | NULL  |         2 |
| 110116033 | Latifa Diniputri                    | NULL      | NULL      | P    |        1 | NULL |     2016 | NULL  |         2 |
| 110116034 | Afifa Diniputri                     | NULL      | NULL      | P    |        1 | NULL |     2016 | NULL  |         2 |
| 110116035 | Lailia Cahya Putri                  | NULL      | NULL      | P    |        1 | NULL |     2016 | NULL  |         2 |
| 110116037 | Azmi Faiz Habibi                    | NULL      | NULL      | L    |        1 | NULL |     2016 | NULL  |         2 |
| 110116038 | Ibrahim Syafiq Musyaffa             | NULL      | NULL      | L    |        1 | NULL |     2016 | NULL  |         2 |
| 110116041 | Haiqal Firdho Ghifari               | NULL      | NULL      | L    |        1 | NULL |     2016 | NULL  |         2 |
| 110116043 | Naila Natalia Aufar                 | NULL      | NULL      | P    |        1 | NULL |     2016 | NULL  |         2 |
| 110116044 | Hulwah Zahidah                      | NULL      | NULL      | P    |        1 | NULL |     2016 | NULL  |         2 |
| 110116047 | Muhammad Ridho Fadhillah            | NULL      | NULL      | L    |        1 | NULL |     2016 | NULL  |         2 |
| 110116048 | Randi                               | NULL      | NULL      | L    |        1 | NULL |     2016 | NULL  |         2 |
| 110116049 | Ade Trisna Wardah                   | NULL      | NULL      | L    |        1 | NULL |     2016 | NULL  |         2 |
| 110116058 | Cica Nur Latifah                    | NULL      | NULL      | P    |        1 | NULL |     2016 | NULL  |         2 |
| 110116059 | Laila Nafila                        | NULL      | NULL      | P    |        1 | NULL |     2016 | NULL  |         2 |
| 110214036 | Ulin Nuha Abdillah                  | NULL      | NULL      | L    |        2 | NULL |     2016 | NULL  |         1 |
| 110216002 | Vindi Pop Ardinoto                  | NULL      | NULL      | L    |        2 | NULL |     2016 | NULL  |         1 |
| 110216003 | Fahmi Hafizul Haq                   | NULL      | NULL      | L    |        2 | NULL |     2016 | NULL  |         1 |
| 110216007 | Andrean Dwi Putra                   | NULL      | NULL      | L    |        2 | NULL |     2016 | NULL  |         1 |
| 110216010 | Anifatul Aufah                      | NULL      | NULL      | L    |        2 | NULL |     2016 | NULL  |         1 |
| 110216011 | Kuati Septiani                      | NULL      | NULL      | L    |        2 | NULL |     2016 | NULL  |         1 |
| 110216015 | Nendi Ilham Munanda Tampubolon      | NULL      | NULL      | L    |        2 | NULL |     2016 | NULL  |         1 |
| 110216017 | Wahab Rahmanto                      | NULL      | NULL      | L    |        2 | NULL |     2016 | NULL  |         1 |
| 110216018 | Muhammad Alwi Mahfud                | NULL      | NULL      | L    |        2 | NULL |     2016 | NULL  |         1 |
| 110216019 | Annisa Tahira                       | NULL      | NULL      | L    |        2 | NULL |     2016 | NULL  |         1 |
| 110216020 | Muhammad fajar saputra              | NULL      | NULL      | L    |        2 | NULL |     2016 | NULL  |         1 |
| 110216022 | Adittya Wicaksono                   | NULL      | NULL      | L    |        2 | NULL |     2016 | NULL  |         1 |
| 110216024 | Rizki Aji Gusti Ekaputra            | NULL      | NULL      | L    |        2 | NULL |     2016 | NULL  |         1 |
| 110216028 | Rizky Hidayat Panjaitan             | NULL      | NULL      | L    |        2 | NULL |     2016 | NULL  |         1 |
| 110216030 | Arius Wanimbo                       | NULL      | NULL      | L    |        2 | NULL |     2016 | NULL  |         1 |
| 110216031 | Satria Suryanegara                  | NULL      | NULL      | L    |        2 | NULL |     2016 | NULL  |         1 |
| 110216035 | Mohammad Akmaluddin Novianto        | NULL      | NULL      | L    |        2 | NULL |     2016 | NULL  |         1 |
| 110216036 | Cartridge Ryan Fraditya             | NULL      | NULL      | L    |        2 | NULL |     2016 | NULL  |         1 |
| 110216037 | Muhammad Fadhil Hilmi               | NULL      | NULL      | L    |        2 | NULL |     2016 | NULL  |         1 |
| 110216040 | Mohammad Reza Nurrahman             | NULL      | NULL      | L    |        2 | NULL |     2016 | NULL  |         1 |
| 110216042 | Muhammad Rafi                       | NULL      | NULL      | L    |        2 | NULL |     2016 | NULL  |         1 |
| 110216044 | Arfian Mulya Pasha                  | NULL      | NULL      | L    |        2 | NULL |     2016 | NULL  |         1 |
| 110216045 | Dimas Agung Sahrul Bayan            | NULL      | NULL      | L    |        2 | NULL |     2016 | NULL  |         1 |
| 110216046 | Arif Ariyanto                       | NULL      | NULL      | L    |        2 | NULL |     2016 | NULL  |         1 |
| 110216048 | Isham Dienurrahman                  | NULL      | NULL      | L    |        2 | NULL |     2016 | NULL  |         1 |
| 110216053 | Mohamad Andika Riedo Pangestu       | NULL      | NULL      | L    |        2 | NULL |     2016 | NULL  |         1 |
| 110216056 | Faiz Khoiron                        | NULL      | NULL      | L    |        2 | NULL |     2016 | NULL  |         1 |
| 110216057 | Iqbal Ajie Wahyudin                 | NULL      | NULL      | L    |        2 | NULL |     2016 | NULL  |         1 |
| 110216058 | Muhammad Rifki Chairil              | NULL      | NULL      | L    |        2 | NULL |     2016 | NULL  |         1 |
| 110216060 | Sukmo Afri Ardisa putro             | NULL      | NULL      | L    |        2 | NULL |     2016 | NULL  |         1 |
| 110216061 | Umair                               | NULL      | NULL      | L    |        2 | NULL |     2016 | NULL  |         1 |
| 110216065 | Haya Rasikhah                       | NULL      | NULL      | P    |        2 | NULL |     2016 | NULL  |         1 |
| 110216068 | Arisy Basyiruddin                   | NULL      | NULL      | L    |        2 | NULL |     2016 | NULL  |         1 |
| 110216070 | Syifa Tazkiy Fauziah                | NULL      | NULL      | P    |        2 | NULL |     2016 | NULL  |         1 |
| 110216075 | Rakha Diasry                        | NULL      | NULL      | L    |        2 | NULL |     2016 | NULL  |         1 |
+-----------+-------------------------------------+-----------+-----------+------+----------+------+----------+-------+-----------+
63 rows in set (0,00 sec)



    */
    require_once "DAO.php";
    class Kegiatan extends DAO
    {
        public function __construct()
        {
            parent::__construct("mahasiswa");
        }

        public function simpan($data){
            $sql = "INSERT INTO ".$this->tableName.
            " (nama,tmp_lahir,tgl_lahir,jk,prodi_id,ipk,thnmasuk,email,rombel_id,nim) ".
            " VALUES (?,?,?,?,?,?,?,?,?,?)";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }

        public function ubah($data){
            $sql = "UPDATE ".$this->tableName.
            " SET nama = ? , tmp_lahir = ?, tgl_lahir = ?,  jk = ? , prodi_id = ? , ipk = ?, thnmasuk = ? , email = ? , rombel_id = ?".
            " WHERE nim=?";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }
        //buat fungsi untuk menampilkan statistik
        public function getStatistik(){
        $sql = "SELECT prodi.nama,COUNT(mahasiswa.jk) as jumlah from prodi
        LEFT JOIN mahasiswa ON prodi.id=mahasiswa.prodi_id
        GROUP BY prodi.nama" ;
        $ps = $this->koneksi->prepare($sql);
        $ps->execute();
        return $ps->fetchAll();
        }

    }
?>
