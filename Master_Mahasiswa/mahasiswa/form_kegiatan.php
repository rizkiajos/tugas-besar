<?php
    include_once 'top.php';
    //panggil file yang melakukan operasi db
    require_once 'db/class_kegiatan.php';
    //buat variabel untuk memanggil class
    $obj_kegiatan = new Kegiatan();
    //buat variabel utk menyimpan id
    $_idedit = $_GET['id'];
    //buat pengecekan apakah datanya ada atau tidak
    if(!empty($_idedit)){
        $data = $obj_kegiatan->findByID($_idedit);
    }else{
        $data = [];
    }
?>
<script src="js/form_validasi_kegiatan.js"></script>
<form name="form_kegiatan" class="form-horizontal" method="POST" action="proses_kegiatan.php">
<fieldset>

<!-- Form Name -->
<legend>Form Name</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nim">NIM</label>
  <div class="col-md-4">
  <input id="nim" name="nim" type="text" placeholder="Masukkan NIM" class="form-control input-md" value="<?php echo $data['nim']?>">

  </div>
</div>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nama">Nama</label>
  <div class="col-md-4">
  <input id="nama" name="nama" type="text" placeholder="Masukkan Nama" class="form-control input-md" value="<?php echo $data['nama']?>">

  </div>
</div>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="tmp_lahir">Tempat Lahir</label>
  <div class="col-md-4">
  <input id="tmp_lahir" name="tmp_lahir" type="text" placeholder="Masukkan Tempat Lahir" class="form-control input-md" value="<?php echo $data['tmp_lahir']?>">

  </div>
</div>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="tgl_lahir">Tanggal Lahir</label>
  <div class="col-md-4">
  <input id="tgl_lahir" name="tgl_lahir" type="text" placeholder="Masukkan Tanggal Lahir" class="form-control input-md" value="<?php echo $data['tgl_lahir']?>">

  </div>
</div>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="jk">Jenis Kelamin</label>
  <div class="col-md-4">
  <input id="jk" name="jk" type="text" placeholder="Masukkan Jenis Kelamin" class="form-control input-md" value="<?php echo $data['jk']?>">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="prodi_id">Prodi Id</label>
  <div class="col-md-4">
  <input id="prodi_id" name="prodi_id" type="text" placeholder="Masukkan Prodi Id" class="form-control input-md" value="<?php echo $data['prodi_id']?>">

  </div>
</div>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="ipk">IPK</label>
  <div class="col-md-4">
  <input id="ipk" name="ipk" type="text" placeholder="Masukkan IPK" class="form-control input-md" value="<?php echo $data['ipk']?>">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="thnmasuk">Tahun Masuk</label>
  <div class="col-md-4">
  <input id="thnmasuk" name="thnmasuk" type="text" placeholder="Masukkan Tahun Masuk" class="form-control input-md" value="<?php echo $data['thnmasuk']?>">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="email">Email</label>
  <div class="col-md-4">
  <input id="email" name="email" type="text" placeholder="Masukkan Email" class="form-control input-md" value="<?php echo $data['email']?>">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="rombel_id">Rombel Id</label>
  <div class="col-md-4">
  <input id="rombel_id" name="rombel_id" type="text" placeholder="Masukkan rombel_id" class="form-control input-md" value="<?php echo $data['rombel_id']?>">

  </div>
</div>
<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="proses"></label>
  <div class="col-md-8">
  <?php
    if(empty($_idedit)){
    ?>
      <input type="submit" name="proses" class="btn btn-success" value="Simpan"/>
    <?php
    }else{
      ?>
      <input type="hidden" name="idedit" value="<?php echo $_idedit?>"/>
      <input type="submit" name="proses" class="btn btn-primary" value="Update"/>
      <input type="submit" name="proses" class="btn btn-danger" value="Hapus"/>
    <?php
    }?>
  </div>
</div>
</fieldset>
</form>

<?php
    include_once 'bottom.php';
?>
