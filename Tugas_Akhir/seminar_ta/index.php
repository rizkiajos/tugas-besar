<?php
include_once 'top.php';
require_once 'db/class_kegiatan.php';
?>
<h2>Daftar Seminar Tugas Akhir</h2>
<div class="panel-header">
    <a class="btn icon-btn btn-success" href="form_kegiatan.php">
    <span class="glyphicon btn-glyphicon glyphicon-plus img-
    circle text-success"></span>
    Tambah Seminar Tugas AKhir
    </a>
</div>
<br>
<div class="panel-header">
    <a class="btn icon-btn btn-success" href="grafik_kegiatan.php">
    <span class="glyphicon btn-glyphicon glyphicon img-
    circle text-success"></span>
    Grafik Nilai Akhir
    </a>
</div>
<?php
$obj = new Kegiatan();
$rows = $obj->getAll();
?>
<!-- Buat code javascript untuk memanggil table dan menggunakan fungsi datatable-->
<script languange="JavaScript">
    $(document).ready(function(){
        $('#example').DataTable();
    });
</script>
<table id="example" class="table table-striped table-bordered"><!-- Beri id pada tag table untuk dideteksi javascript-->
    <thead>
    <tr class="active">
        <th>Id</th><th>Tanggal</th><th>Nilai</th><th>Tempat</th><th>Tugas Akhir Id</th><th>Dosen Penguji</th><th>Kategori Seminar Id</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $nomor = 1;
    foreach($rows as $row){
        echo '<tr><td>'.$nomor.'</td>';
        echo '<td>'.$row['tanggal'].'</td>';
        echo '<td>'.$row['nilai'].'</td>';
        echo '<td>'.$row['tempat'].'</td>';
        echo '<td>'.$row['tugasakhir_id'].'</td>';
        echo '<td>'.$row['dosen_penguji'].'</td>';
        echo '<td>'.$row['kategori_seminar_id'].'</td>';
        echo '<td><a href="view_kegiatan.php?id='.$row['id']. '">View</a> |';
        echo '<a href="form_kegiatan.php?id='.$row['id']. '">Update</a></td>';
        echo '</tr>';
        $nomor++;
    }
    ?>
    </tbody>
</table>
<?php
include_once 'bottom.php';
?>
