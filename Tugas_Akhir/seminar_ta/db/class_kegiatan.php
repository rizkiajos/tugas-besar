<?php
    /*
    mysql> select *from seminar_ta;
+----+------------+-------+--------+---------------+---------------+---------------------+
| id | tanggal    | nilai | tempat | tugasakhir_id | dosen_penguji | kategori_seminar_id |
+----+------------+-------+--------+---------------+---------------+---------------------+
|  1 | 2017-04-20 |     0 | B2 204 |             1 |            20 |                   1 |
|  2 | 2017-05-05 |     0 | B2 203 |             2 |            17 |                   1 |
|  3 | 2017-05-04 |     0 | B2 203 |             2 |            14 |                   1 |
+----+------------+-------+--------+---------------+---------------+---------------------+
3 rows in set (0,00 sec)


    */
    require_once "DAO.php";
    class Kegiatan extends DAO
    {
        public function __construct()
        {
            parent::__construct("seminar_ta");
        }

        public function simpan($data){
            $sql = "INSERT INTO ".$this->tableName.
            " (id,tanggal , nilai , tempat , tugasakhir_id , dosen_penguji , kategori_seminar_id) ".
            " VALUES (default,?,?,?,?,?,?)";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }

        public function ubah($data){
            $sql = "UPDATE ".$this->tableName.
            " SET tanggal=?,nilai=?,tempat=?,tugasakhir_id=?,dosen_penguji=? , kategori_seminar_id ".
            " WHERE id=?";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }
        //buat fungsi untuk menampilkan statistik
        public function getStatistik(){
        $sql = "SELECT tugasakhir.nilai, tugasakhir.nim as nim FROM seminar_ta LEFT JOIN tugasakhir ON seminar_ta.tugasakhir_id=tugasakhir.id GROUP BY seminar_ta.tugasakhir_id" ;
        $ps = $this->koneksi->prepare($sql);
        $ps->execute();
        return $ps->fetchAll();
        }
    }
?>