<?php
include_once 'top.php';
require_once 'db/class_kegiatan.php';
?>
<h2>Daftar Tugas Akhir</h2>
<div class="panel-header">
    <a class="btn icon-btn btn-success" href="form_kegiatan.php">
    <span class="glyphicon btn-glyphicon glyphicon-plus img-
    circle text-success"></span>
    Tambah Tugas Akhir
    </a>
</div>
<br>

<?php
$obj = new Kegiatan();
$rows = $obj->getAll();
?>
<!-- Buat code javascript untuk memanggil table dan menggunakan fungsi datatable-->
<script languange="JavaScript">
    $(document).ready(function(){
        $('#example').DataTable();
    });
</script>
<table id="example" class="table table-striped table-bordered"><!-- Beri id pada tag table untuk dideteksi javascript-->
    <thead>
    <tr class="active">
        <th>Id</th><th>Judul</th><th>Semester</th><th>NIM</th><th>Dosen Pembimbing</th><th>Nilai</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $nomor = 1;
    foreach($rows as $row){
        echo '<tr><td>'.$nomor.'</td>';
        echo '<td>'.$row['judul'].'</td>';
        echo '<td>'.$row['semester'].'</td>';
        echo '<td>'.$row['nim'].'</td>';
        echo '<td>'.$row['dosen_pembimbing'].'</td>';
        echo '<td>'.$row['nilai'].'</td>';
        echo '<td><a href="view_kegiatan.php?id='.$row['id']. '">View</a> |';
        echo '<a href="form_kegiatan.php?id='.$row['id']. '">Update</a></td>';
        echo '</tr>';
        $nomor++;
    }
    ?>
    </tbody>
</table>
<?php
include_once 'bottom.php';
?>
