<?php
    /*
    mysql> select * from tugasakhir;
+----+------------------------------------------------------------+----------+-----------+------------------+-------+
| id | judul                                                      | semester | nim       | dosen_pembimbing | nilai |
+----+------------------------------------------------------------+----------+-----------+------------------+-------+
|  1 | Perancangan eVoting berbasis Mobile                        |    20181 | 110116026 |               17 |  NULL |
|  2 | Implementasi Data Mining PT ABC                            |    20181 | 110216011 |                6 |  NULL |
|  3 | Pemanfaatan Social Media Pada Kampanye Gubernur Jabar 2018 |    20181 | 110116023 |               16 |  NULL |
+----+------------------------------------------------------------+----------+-----------+------------------+-------+
3 rows in set (0,00 sec)



    */
    require_once "DAO.php";
    class Kegiatan extends DAO
    {
        public function __construct()
        {
            parent::__construct("tugasakhir");
        }

        public function simpan($data){
            $sql = "INSERT INTO ".$this->tableName.
            " (id,judul,semester,nim,dosen_pembimbing,nilai) ".
            " VALUES (default,?,?,?,?,?)";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }

        public function ubah($data){
            $sql = "UPDATE ".$this->tableName.
            " SET judul = ? ,  semester = ?,  nim = ?,  dosen_pembimbing = ? ,  nilai = ?".
            " WHERE id=?";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }
        //buat fungsi untuk menampilkan statistik

    }
?>
