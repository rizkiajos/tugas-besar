<?php
    include_once 'top.php';
    //panggil file yang melakukan operasi db
    require_once 'db/class_kegiatan.php';
    //buat variabel untuk memanggil class
    $obj_kegiatan = new Kegiatan();
    //buat variabel utk menyimpan id
    $_idedit = $_GET['id'];
    //buat pengecekan apakah datanya ada atau tidak
    if(!empty($_idedit)){
        $data = $obj_kegiatan->findByID($_idedit);
    }else{
        $data = [];
    }
?>
<script src="js/form_validasi_kegiatan.js"></script>
<form name="form_kegiatan" class="form-horizontal" method="POST" action="proses_kegiatan.php">
<fieldset>

<!-- Form Name -->
<legend>Form Name</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="kode">Id</label>
  <div class="col-md-4">
  <input id="id" name="id" type="text" placeholder="Masukkan Id" class="form-control input-md" value="<?php echo $data['id']?>">

  </div>
</div>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="kode">Judul</label>
  <div class="col-md-4">
  <input id="judul" name="judul" type="text" placeholder="Masukkan Judul" class="form-control input-md" value="<?php echo $data['judul']?>">

  </div>
</div>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="kode">Semester</label>
  <div class="col-md-4">
  <input id="semester" name="semester" type="text" placeholder="Masukkan Semester" class="form-control input-md" value="<?php echo $data['semester']?>">

  </div>
</div>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="kode">NIM</label>
  <div class="col-md-4">
  <input id="nim" name="nim" type="text" placeholder="Masukkan NIM" class="form-control input-md" value="<?php echo $data['nim']?>">

  </div>
</div>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="kode">Dosen Pembimbing</label>
  <div class="col-md-4">
  <input id="dosen_pembimbing" name="dosen_pembimbing" type="text" placeholder="Masukkan Dosen Pembimbing" class="form-control input-md" value="<?php echo $data['dosen_pembimbing']?>">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="kode">Nilai</label>
  <div class="col-md-4">
  <input id="nilai" name="nilai" type="text" placeholder="Masukkan nilai" class="form-control input-md" value="<?php echo $data['nilai']?>">

  </div>
</div>


<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="proses"></label>
  <div class="col-md-8">
  <?php
    if(empty($_idedit)){
    ?>
      <input type="submit" name="proses" class="btn btn-success" value="Simpan"/>
    <?php
    }else{
      ?>
      <input type="hidden" name="idedit" value="<?php echo $_idedit?>"/>
      <input type="submit" name="proses" class="btn btn-primary" value="Update"/>
      <input type="submit" name="proses" class="btn btn-danger" value="Hapus"/>
    <?php
    }?>
  </div>
</div>
</fieldset>
</form>

<?php
    include_once 'bottom.php';
?>
