<?php
    /* mysql> select *from kategori_seminar;
+----+------------------+
| id | nama             |
+----+------------------+
|  1 | Proposal TA      |
|  2 | Seminar Hasil TA |
|  3 | Sidang TA        |
+----+------------------+
3 rows in set (0,00 sec)
*/

    require_once "DAO.php";
    class Kegiatan extends DAO
    {
        public function __construct()
        {
            parent::__construct("kategori_seminar");
        }

        public function simpan($data){
            $sql = "INSERT INTO ".$this->tableName.
            " (id,nama) ".
            " VALUES (default,?)";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }

        public function ubah($data){
            $sql = "UPDATE ".$this->tableName.
            " SET nama=?".
            " WHERE id=?";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }
        //buat fungsi untuk menampilkan statistik

    }
?>